﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

namespace HorribleSubsScraper
{
    public class DownloadFile
    {
        public void downloadTorrents(List<string[]> list)
        {
            int countD = 0;
            int countS = 0;
            for (int i = 0; i < list.Count; i++)
            {
                try
                {
                    using (var client = new WebClient())
                    {
                        client.Encoding = Encoding.UTF8;
                        if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\" + list[i][0]))
                        {
                            Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\" + list[i][0]);
                        }
                        if (!File.Exists(Directory.GetCurrentDirectory() + "\\" + list[i][0] + "\\" + list[i][2] + ".torrent"))
                        {
                            client.DownloadFile(list[i][1], Directory.GetCurrentDirectory() + "\\" + list[i][0] + "\\" + list[i][2] + ".torrent");
                            Console.Write("\rDownloaded torrent: " + ++countD);
                        }
                        else
                        {
                            ++countS;
                        }
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Failed to download torrent file: " + list[i][2] + ".torrent\nDo you have a internet connection?");
                }
            }
            Console.WriteLine("Torrents skipped, due to previous download: " + countS);
        }
    }
}
