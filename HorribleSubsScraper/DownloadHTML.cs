﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

namespace HorribleSubsScraper
{
    public class DownloadHTML
    {
        public string getAnimes()
        {
            string link = "https://horriblesubs.info/shows/";
            string html = "";
            try
            {
                using (var client = new WebClient())
                {
                    client.Encoding = Encoding.UTF8;
                    html = client.DownloadString(link);
                }
                Console.WriteLine("HTML downloaded for list of anime shows.");
            }
            catch (Exception)
            {
                Console.WriteLine("Failed to retrieve HTML for anime: " + link + "\nDo you have a internet connection?");
                return null;
            }
            return html;
        }

        public List<string> getMagnets50()
        {

            List<string> magnetHTML = new List<string>();
            for (int i = 1; i <= 10; i++)
            {
                string link = "https://nyaa.si/user/HorribleSubs?p=" + i;
                try
                {
                    using (var client = new WebClient())
                    {
                        client.Encoding = Encoding.UTF8;
                        magnetHTML.Add(client.DownloadString(link));
                        Console.Write("\rDownloaded pages from Nyaa.si: " + i);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed to retrieve HTML for magnets: " + link + "\nDo you have a internet connection?\n\n" + ex.Message);
                }
            }
            Console.WriteLine("");
            return magnetHTML;
        }

        public List<string> getMagnets()
        {
            int pageNumber = 1;
            string link = "https://nyaa.si/user/HorribleSubs?p=" + pageNumber;
            List<string> magnetHTML = new List<string>();
            bool continueNextPage = true;
            do
            {
                try
                {
                    using (var client = new WebClient())
                    {
                        client.Encoding = Encoding.UTF8;
                        string html = client.DownloadString(link);
                        if (html.Contains("404 Not Found"))
                        {
                            continueNextPage = false;
                        }
                        else
                        {
                            magnetHTML.Add(html);
                            Console.Write("\rDownloaded pages from Nyaa.si: " + pageNumber);
                            pageNumber++;
                        }
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Failed to retrieve HTML for magnets: " + link + "\nDo you have a internet connection?");
                }
            } while (continueNextPage);
            Console.WriteLine("");
            return magnetHTML;
        }
    }
}
