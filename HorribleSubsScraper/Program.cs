﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HorribleSubsScraper
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("HorribleSubsScraper v0.1 by Coenraad Human\n");
            DownloadHTML downloadHTML = new DownloadHTML();
            ScrapeFile scrape = new ScrapeFile();
            StoreToDrive save = new StoreToDrive();
            DownloadFile downloadFile = new DownloadFile();
            downloadFile.downloadTorrents(scrape.scrapeMagnets(downloadHTML.getMagnets50()));
            Console.WriteLine("Files stored in " + Directory.GetCurrentDirectory());
            Console.WriteLine("Finito, press any key to close console.");
            Console.ReadKey();
        }
    }
}
