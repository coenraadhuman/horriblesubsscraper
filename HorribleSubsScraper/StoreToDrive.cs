﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HorribleSubsScraper
{
    public class StoreToDrive
    {
        public void save(string path, List<List<string>> list)
        {
            string answer;
            bool boolAnswer = true;
            do
            {
                Console.Write("Would you like to save downloaded information (y/n): ");
                answer = Console.ReadLine().ToString();
                if (answer == "y" || answer == "n")
                {
                    boolAnswer = false;
                }
            } while (boolAnswer);
            if (answer == "y")
            {
                Console.WriteLine("Data was stored to: " + Directory.GetCurrentDirectory());
                for (int i = 0; i < list.Count; i++)
                {
                    string storePath = Directory.GetCurrentDirectory() + "\\" +list[i][0];
                    if (!Directory.Exists(storePath))
                    {
                        Directory.CreateDirectory(storePath);
                    }
                    using (StreamWriter writer = new StreamWriter(storePath + "\\Description.txt"))
                    {
                        writer.WriteLine(list[i][2]);
                    }
                    for (int j = 4; j < list[i].Count; j = j + 4)
                    {
                        using (StreamWriter writer = new StreamWriter(storePath + "\\Episode_" + list[j] + ".txt"))
                        {
                            writer.WriteLine("Episode " + list[i][j] + " of " + list[i][0]);
                            writer.WriteLine(" ");
                            writer.WriteLine("480p Magnet: ");
                            writer.WriteLine("Episode " + list[i][j + 1] + " of " + list[i][0]);
                            writer.WriteLine(" ");
                            writer.WriteLine("720p Magnet: ");
                            writer.WriteLine("Episode " + list[i][j + 2] + " of " + list[i][0]);
                            writer.WriteLine(" ");
                            writer.WriteLine("1080p Magnet: ");
                            writer.WriteLine("Episode " + list[i][j + 3] + " of " + list[i][0]);
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Data was NOT stored!");
            }
        }
    }
}
