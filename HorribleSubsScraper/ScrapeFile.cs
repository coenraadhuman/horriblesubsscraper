﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HorribleSubsScraper
{
    public class ScrapeFile
    {
        public List<List<string>> getAnimeShows(string html)
        {
            if (!string.IsNullOrWhiteSpace(html))
            {
                List<List<string>> animeShows = new List<List<string>>();
                try
                {
                    int show = 1;
                    string htmlCopy = html;
                    htmlCopy = htmlCopy.Substring(htmlCopy.IndexOf("ind-show") - 12);
                    htmlCopy = htmlCopy.Remove(htmlCopy.LastIndexOf("ind-show") + 275);
                    while (htmlCopy.Contains("ind-show"))
                    {
                        // Get show line
                        string line = htmlCopy.Substring(htmlCopy.IndexOf("ind-show") - 12);
                        line = line.Remove(line.IndexOf("/div>") + 5);
                        // Remove line from htmlCopy
                        htmlCopy = htmlCopy.Replace(line, "");
                        if (line.Contains("title"))
                        {
                            /* arr:
                             * 0 - Show Name
                             * 1 - HorribleSubs Link For Show
                             * 2 - Show Description
                             * 3 - Show Image URL
                             * Preceding order:
                             * Episode Number
                             * 480p Magnet
                             * 720p Magnet
                             * 1080p Magnet
                             */
                            List<string> arr = new List<string>();
                            arr[0] = line.Remove(line.IndexOf("</a>"));
                            arr[0] = arr[0].Substring(arr[0].LastIndexOf(">") + 1);
                            arr[1] = line.Remove(line.IndexOf("</a>"));
                            arr[1] = arr[1].Remove(arr[1].LastIndexOf("title") - 2);
                            arr[1] = arr[1].Substring(arr[1].IndexOf("href") + 6);
                            arr[1] = "https://horriblesubs.info" + arr[1];
                            animeShows.Add(arr);
                            Console.Write("\rAnime show links retrieved from HorribleSubs: {0}", show++);
                        }
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Something went wrong with the string manipulation for anime shows.\nHorrible might have changed there site...");
                }
                return animeShows;
            }
            else
            {
                return null;
            }
        }

        public List<string[]> scrapeMagnets(List<string> listHTML)
        {
            List<string[]> list = new List<string[]>();
            int countS = 0;
            int countF = 0;
            try
            {
                for (int i = 0; i < listHTML.Count; i++)
                {
                    listHTML[i] = listHTML[i].Substring(listHTML[i].IndexOf("<tbody>"));
                    listHTML[i] = listHTML[i].Remove(listHTML[i].LastIndexOf("color: red;"));
                    do
                    {
                        /* arr:
                         * 0 Anime Show
                         * 1 Torrent
                         * 2 Torrent File Name
                         */
                        string[] arr = new string[3];
                        listHTML[i] = listHTML[i].Substring(listHTML[i].IndexOf("<a href"));
                        string lineA1 = listHTML[i].Remove(listHTML[i].IndexOf(">") + 1);
                        listHTML[i] = listHTML[i].Replace(lineA1, "");
                        // Contains anime show name, episode number and quality
                        listHTML[i] = listHTML[i].Substring(listHTML[i].IndexOf("title=\"[HorribleSubs]") - 9);
                        string lineA2 = listHTML[i].Remove(listHTML[i].IndexOf("</a>") + 4);
                        listHTML[i] = listHTML[i].Replace(lineA2, "");
                        // Torrent URL
                        listHTML[i] = listHTML[i].Substring(listHTML[i].IndexOf("/download/"));
                        string lineA3 = listHTML[i].Remove(listHTML[i].IndexOf(".torrent") + 8);
                        lineA3 = "https://nyaa.si" + lineA3;
                        listHTML[i] = listHTML[i].Substring(listHTML[i].IndexOf("=\"success") + 9);
                        string compare1 = lineA2.Remove(7);
                        string compare2 = lineA3.Remove(lineA3.IndexOf(".torrent"));
                        compare2 = compare2.Substring(compare2.LastIndexOf("/") + 1);
                        if (compare1 == compare2)
                        {
                            Console.Write("\rTorrents processed of HorribleSubs from Nyaa.si: " + ++countS);
                            arr[1] = lineA3;
                            arr[2] = lineA2.Remove(lineA2.LastIndexOf("p]") + 2);
                            arr[2] = arr[2].Substring(arr[2].LastIndexOf("HorribleSubs") + 14);
                            arr[0] = arr[2].Remove(arr[2].LastIndexOf("-") - 1);
                            list.Add(arr);
                        }
                        else
                        {
                            ++countF;
                        }
                    } while (listHTML[i].Contains("=\"success"));
                }
                Console.WriteLine("\nTorrents failed to process correctly: " + countF);
            }
            catch (Exception)
            {
                Console.WriteLine("\nSomething went wrong with the string manipulation for anime shows.\nHorrible might have changed there site...");
            }
            return list;
        }
    }
}
