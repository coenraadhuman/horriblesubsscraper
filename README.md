HorribleSubsScraper
===================

Simple [self-contained](https://stackify.com/cross-platform-net-core-apps/) [.Net Core](https://github.com/dotnet/core) application to retrieve torrents for anime by [HorribleSubs](https://horriblesubs.info/).

Releases
--------
- [Version 0.1](http://bit.ly/2N2boZd): Downloads first 10 pages from Nyaa.si for HorribleSubs and download torrents on pages.

System requirements
----------------------------
- 64 bit
- Linux, Windows or Mac

Instructions - Windows
------------------------------
- Download 'release'.7zip and extract it.
- Use HorribleSubsScraper.exe to execute program.

Instructions - Linux/Mac
------------------------------
- Download 'release'.7zip and extract it.
- Now make the program executable:
```shell
sudo chmod +x ./HorribleSubsScraper
```
- To execute program, from terminal within located directory of program:
```shell
./HorribleSubsScraper
```

![READMELOGO](https://gitlab.com/coenraadhuman/HorribleSubsScraper/raw/master/READMELOGO.jpg)
